//Michael Dougherty
//Lucky Sevens V1

function playLuckySevens()
{
	//Convert a string of "$#.##" to a float of #.##
	var startingBet = document.getElementById("startingBet").value;
	startingBet = startingBet.replace("$", "");
	startingBet = Math.floor(parseFloat(startingBet));
	
	
	var currentAmount = startingBet;
	var maximumAmount = startingBet;
	
	var currentNumberOfRolls = 0;
	var numberOfRollsAtMaxAmount = 0;
	
	
	//The dice for the game.
	var die_01, die_02;
	
	var sumOfDice;
	
	//Input Validation
	if(startingBet <= 0)
	{
		window.alert("The starting bet cannot be less than or equal to zero. Please enter another number.");
	}
	else
	{
		do
		{
			die_01 = Math.floor(Math.random() * 6 ) + 1;
			die_02 = Math.floor(Math.random() * 6 ) + 1;
			
			sumOfDice = die_01 + die_02;
			currentNumberOfRolls++;
			
			if(sumOfDice == 7)
			{
				currentAmount += 4;
			}
			else
			{
				currentAmount--;
			}
			
			//Determine the maximum amount of money and number of dice rolls at maximum amount.
			if(currentAmount > maximumAmount)
			{
				maximumAmount = currentAmount;
				numberOfRollsAtMaxAmount = currentNumberOfRolls;
			}
			
		} while(currentAmount > 0);
	}
	
	hideStartScreen();
	
	//Set the elements of the results table with the appropriate data.
	document.getElementById("outputStartingBet").innerHTML = "$" + String(startingBet);
	document.getElementById("outputCurrentNumberOfRolls").innerHTML = String(currentNumberOfRolls);
	document.getElementById("outputMaximumAmount").innerHTML = "$" + String(maximumAmount);
	document.getElementById("outputNumberOfRollsAtMaxAmount").innerHTML = String(numberOfRollsAtMaxAmount);
	
	showReplayScreen();
}
			
function doLoadStuff()
{
	hideReplayScreen();
}
			
function hideReplayScreen()
{
	var replayScreen = document.getElementsByName("replayScreen");
	for (var index = 0; index < replayScreen.length; index++)
	{
		replayScreen[index].style.display = "none";
	}
}
			
function showReplayScreen()
{
	var replayScreen = document.getElementsByName("replayScreen");
	for (var index = 0; index < replayScreen.length; index++)
	{
		replayScreen[index].style.display = "block";
	}
}
			
function hideStartScreen()
{
	var startScreen = document.getElementsByName("startScreen");
	for (var index = 0; index < startScreen.length; index++)
	{
		startScreen[index].style.display = "none";
	}
}
			